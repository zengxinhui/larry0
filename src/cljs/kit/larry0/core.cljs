(ns kit.larry0.core
  (:require
   [reagent.core :as r]
   [reagent.dom :as d]))

;; answered correctly queue. 1 correct 0 wrong
(def stats1 (r/atom []))
;; time spent
(def stats2 (r/atom []))
;; question queue
(def stats3 (r/atom []))
;; clock related
(def seconds-elapsed (r/atom 0))
(def timeout-id (js/setInterval #(swap! seconds-elapsed inc) 1000))

;; -------------------------
;; Views
(defn time-component []
  [:div (str "Time: " @seconds-elapsed " second(s)")])

(defn home-page []
  (let [x (rand-int 100)
        y (rand-int 100)
        timer (js/Date.)
        handler (fn [e]
                  (let [v (js/parseInt (.-value e))]
                    (swap! stats1 conj (if (= v (+ x y)) 1 0))
                    (swap! stats2 conj (- (js/Date.) timer))
                    (swap! stats3 conj (str x " + " y " = " v))
                    (set! (.-value e) "")
                    (reset! seconds-elapsed 0)))
        handle-enter (fn [e] (when (= (.-key e) "Enter") (handler (.-target e))))
        handle-submit (fn [e] (handler (.getElementById js/document "answer")))]
    [:div
     (apply + @stats1) "/" (count @stats1)
     ", total: " (apply + @stats2)
     ", avg: " (if (seq @stats2) (int (/ (apply + @stats2) (count @stats2))) "")
     ", best: " (reduce min @stats2)
     ", worst: " (reduce max @stats2)
     [time-component]
     [:div x " + " y " = "
      [:input {:type "tel" :id "answer" :on-key-down handle-enter :tabindex 0}]
      [:input {:type "submit" :value "submit" :on-click handle-submit}]]
     (->> (map #(if (< 0 %) 
                  [:div.green-square {:title %2}]
                  [:div.red-square {:title %2}])
               @stats1 @stats2)
          (into [:div.container]))
     (->> (map #(if (< 0 %) [:li %2] [:li.red %2])
               (reverse @stats1) (reverse @stats3))
          (into [:ul]))]))

;; -------------------------
;; Initialize app

(defn ^:dev/after-load mount-root []
  (d/render [home-page] (.getElementById js/document "app"))
  (.focus (.getElementById js/document "answer")))

(defn ^:export ^:dev/once init! []
  (mount-root))
