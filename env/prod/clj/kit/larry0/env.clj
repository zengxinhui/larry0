(ns kit.larry0.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[larry0 starting]=-"))
   :start      (fn []
                 (log/info "\n-=[larry0 started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[larry0 has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
