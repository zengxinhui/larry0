(ns kit.larry0.env
  (:require
    [clojure.tools.logging :as log]
    [kit.larry0.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[larry0 starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[larry0 started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[larry0 has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
